/*
 * Задача 4: «С большой буквы»
 *
 * Напишите функцию capitalize(str). Функция должна возвращать новую
 * строку каждое слово в которой начинается с прописной буквы.
 *
*/

function capitalize(str) {
    let result = str.split('').map(function(obj, iter){
        if (iter === 0 || (iter !== ' ' && str[iter -1] === ' ')) {
            obj = obj.toUpperCase();
        }
        return obj;
    })
    
    return result.join('');
}

// Протестируйте решение, вызывая функцию с разными аргументами:

console.log(capitalize('молодость всё простит')); // "Молодость Всё Простит"