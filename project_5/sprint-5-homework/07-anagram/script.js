/*
 * Задача 7: «Анаграмма»
 *
 * Два слова называют анаграммами, если они состоят из одних и тех же букв.
 * Напишите функцию, проверяющую, являются ли две строки анаграммами друг друга
 * (регистр букв не имеет значения). Для простоты примите, что в этих строках
 * нет пробелов и знаков препинания.
 * 
*/

function anagram(str1, str2) {

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 Если валидация с входными данными 'up' 'UP' верна и функция должна 
 возвращать true основываясь на цитате ниже, просьба указать этот нюанс в объяснении задачи, 
 дабы будущие поколения не тратили уйму времени для выяснения этого как это делал я.   
 
    АНАГРА́ММА
    Женский родСПЕЦИАЛЬНОЕ
    Слово или словосочетание, образованное путём перестановки букв, составляющих другое слово (или словосочетание).
*/

    let result = false;

    if (str1.length === str2.length && str1.toLowerCase() !== str2.toLowerCase()) {
        str1 = str1.toLowerCase().split('')
        result = str1.every(function(obj){
            return str2.toLowerCase().includes(obj);
        })
    }

    return result
}

// Протестируйте решение, вызывая функцию с разными аргументами:

console.log(anagram('finder', 'Friend')); // true
console.log(anagram('hello', 'bye')); // false