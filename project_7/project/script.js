//                                                               Переменные
// Можно лучше (минута делов):   перенесите  в отдельный файл, меньше строк, больше понимание, 
// Правильная организация кода, это важная часть разработки. Ведь код надо постоянно поддерживать
// подключить его можно через  <script src="js/initialCards.js"></script> 
// Плюс мы выносим данные отдельно, а логика останется в этом файле 
const initialCards = [
    {
        name: 'Архыз',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/arkhyz.jpg'
    },
    {
        name: 'Челябинская область',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/chelyabinsk-oblast.jpg'
    },
    {
        name: 'Иваново',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/ivanovo.jpg'
    },
    {
        name: 'Камчатка',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kamchatka.jpg'
    },
    {
        name: 'Холмогорский район',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kholmogorsky-rayon.jpg'
    },
    {
        name: 'Байкал',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/baikal.jpg'
    },
    {
        name: 'Нургуш',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/khrebet-nurgush.jpg'
    },
    {
        name: 'Тулиновка',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/tulinovka.jpg'
    },
    {
        name: 'Остров Желтухина',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/zheltukhin-island.jpg'
    },
    {
        name: 'Владивосток',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/vladivostok.jpg'
    }
];
const root = document.querySelector('.root');                   
const placesList = document.querySelector('.places-list');      
const infoName = root.querySelector('.user-info__name');        // Имя пользователя
const infoJob = root.querySelector('.user-info__job');          // Деятельность пользователя
const addCardBtn = root.querySelector('.user-info__button');    // Кнопка создания карточки
const editBtn = root.querySelector('.user-info__button_edit');  // Кнопка редактирования пользователя

const popupNewCard = root.querySelector('#newCard');            // Модальное окно создание карточки
const popupEditProfile = root.querySelector('#editProfile');    // Модальное окно редактирование профиля
const popupViewImage = root.querySelector('#viewImage');        // Модальное окно картинка карточки

let activePopup


//                                                                Функции

// создание карточки
function createCard(name, link) {
    placeCard = `<div class='place-card'>
                    <div class='place-card__image' style='background-image:url(${link})'>
                        <button class='place-card__delete-icon'></button>
                    </div>
                    <div class='place-card__description'>
                        <h3 class='place-card__name'>${name}</h3>
                        <button class='place-card__like-icon'></button>
                   </div>
                </div>`

    placesList.insertAdjacentHTML('beforeend', placeCard);
};

// Открытие модалки с необходимой формой
function openPopup(popupOBJ) {
    popupOBJ.classList.add('popup_is-opened');
    activePopup = popupOBJ;
};

// Закрытие модалки с необходимой формой
function closePopup() {
    activePopup.classList.remove('popup_is-opened');
    const popupForm = activePopup.querySelector(".popup__form");
    popupForm ? popupForm.reset() : null;
};

function checkInputValidity(inputElement, errorElement) {
    //функция валидации поля. Она принимает два аргумента: элемент поля и элемент ошибки. 
    //Если поле прошло валидацию, ошибку следует скрыть. Если не прошло — показать.
    console.log(inputElement, inputElement.validationMessage)
    if (inputElement.validationMessage) {
        errorElement.textContent = inputElement.validationMessage;
        return false};

    errorElement.textContent = '';
    return true
};

function setSubmitButtonState(btnObj, btnIsActive) {
    //функция, меняющая состояние кнопки сабмита. Состояние кнопки сабмита зависит от того, 
    //прошли все поля валидацию или нет. Определите самостоятельно, какие аргументы передавать этой функции.
    btnObj.disabled = !btnIsActive;
};

function setEventListeners() {
    //функция добавления обработчиков. Принимает единственный аргумент — элемент попапа. 
    //Добавляет необходимые обработчики всем его полям. Эта функция в своём теле 
    //вызывает функции checkInputValidity и setSubmitButtonState.
    const popupForm = activePopup.querySelector('.popup__form')
    let formElements = Array.from(popupForm);              // получаем все её элементы
    const submitBtn = popupForm.querySelector('.button');  // получаем сабмит формы

    popupForm.addEventListener('input', function(){
        let formIsValid = true;
        formElements.forEach((element) => {     
            if (element.type !== 'submit'){                // обрабатываем все поля отличные от submit
                if (!checkInputValidity(element, activePopup.querySelector(`#error-${element.name}`))){
                    formIsValid = false;
                };
            };
        });
        setSubmitButtonState(submitBtn, formIsValid);
    });
};

//                                                               Обработчики
// открытие попапов
root.addEventListener('click', function(event){
    if (event.target === addCardBtn){
        openPopup(popupNewCard);
        setEventListeners();
    } else if (event.target === editBtn) {
        openPopup(popupEditProfile);
        document.forms.editProfile.userName.value = infoName.textContent
        document.forms.editProfile.userJob.value = infoJob.textContent
        setEventListeners();
    } else if (event.target.classList.contains("place-card__image")) {
        openPopup(popupViewImage);
        const imgUrl = event.target.style['background-image'];
        popupViewImage.classList.add('popup_is-opened');
        popupViewImage.querySelector('.popup__image').style.backgroundImage = `${imgUrl}`;
    }
})

// работа с карточками
root.addEventListener('click', function(event) {
    if (event.target.classList.contains('place-card__like-icon')) {             // Лайк
        event.target.classList.toggle('place-card__like-icon_liked');
    } else if (event.target.classList.contains('place-card__delete-icon')) {    // Удаление карточки
        var delPlaceCard = event.target.parentElement.parentElement
        placesList.removeChild(delPlaceCard);
    }
});

// закрываем модалку крестиком
root.addEventListener('click', function(event) {
    if (event.target.classList.contains('popup__close')) {
        closePopup();
    }
});

// закрываем модалку клавишей esc
root.addEventListener('keydown', function(event) {
    // if (activePopup && event.key === 'Escape') {
    if (event.key === 'Escape') {
        console.log(activePopup);
        console.log(activePopup && event.key === 'Escape');
        closePopup();
    }
});

// работа с сабмитами
root.addEventListener('submit', function(event) {
    event.preventDefault();
    if (event.target.getAttribute('name') == 'newCard') {
        const name = document.forms.newCard.elements.name.value;
        const link = document.forms.newCard.elements.link.value;
        createCard(name, link);
        closePopup();
    }

    if (event.target.getAttribute('name') == 'editProfile') {
        infoName.textContent = document.forms.editProfile.elements.userName.value;
        infoJob.textContent = document.forms.editProfile.elements.userJob.value;
        closePopup();
    }
});
//                                                              Вызов функций

// генерируем карточки
initialCards.forEach(function(obj){
    createCard(obj.name, obj.link);
})


 /**
 * Здравствуйте.
 * --------------------------------------------------------------------
 * Весь функционал работает корректно 
 * Код чистый и хорошо читается 
 * Вы используете логические группировки операций 
 * У вас нет дублирование кода
 *  Вы не используете небезопасный innerHtml
 *  Вы используете делегирование
 * -------------------------------------------------------------------- 
    
  * можно лучше: избегайте сложных условий и большой вложенности в условии. Чем сложнее условие, чем больше
  * вложенности в условии, тем сложнее анализировать код, сложнее искать ошибки и поддерживать такой код
  * самый простой вариант это убирать условия или блок в условии в отдельную функцию
 *
 * можно лучше: Старайтесь не объявлять большое количество переменных. Чем больше переменных, тем сложнее понимать за что они 
 * отвечают и какую полезную нагрузку несут в коде. Лучше инкапсулировать(прятать) переменные в функциях. 
 * В будущем вам проще будет искать ошибки и разбираться в сложных взаимосвязях
 * 
 * 
   Перед сдачей работы на проверку или публикацией 
 удаляйте console.log() везде 
 console.log() используется только для разработки.

    Работа принимается

 
 * 
 */