const initialCards = [
    {
        name: 'Архыз',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/arkhyz.jpg'
    },
    {
        name: 'Челябинская область',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/chelyabinsk-oblast.jpg'
    },
    {
        name: 'Иваново',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/ivanovo.jpg'
    },
    {
        name: 'Камчатка',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kamchatka.jpg'
    },
    {
        name: 'Холмогорский район',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/kholmogorsky-rayon.jpg'
    },
    {
        name: 'Байкал',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/baikal.jpg'
    },
    {
        name: 'Нургуш',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/khrebet-nurgush.jpg'
    },
    {
        name: 'Тулиновка',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/tulinovka.jpg'
    },
    {
        name: 'Остров Желтухина',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/zheltukhin-island.jpg'
    },
    {
        name: 'Владивосток',
        link: 'https://pictures.s3.yandex.net/frontend-developer/cards-compressed/vladivostok.jpg'
    }
];

const placesList = document.querySelector('.places-list');
const root = document.querySelector('.root');
const popup = document.querySelector('.popup');
//Можно лучше: Неиспользуемые переменные целесообразно удалять из кода, чтобы не засорять его.
const btnLike = document.querySelector('.place-card__like-icon');
const popupForm = document.forms.new;

// ---------------------- 1. Десять карточек «из коробки» ----------------------
function createCard(name, link) {
    /*
    <div class='place-card'>
        <!-- Картинка фоном в этот элемент -->
        <div class='place-card__image'>
            <button class='place-card__delete-icon'></button>
        </div>
        <div class='place-card__description'>
            <!-- А это элемент для названия -->
            <h3 class='place-card__name'>Камчатка</h3>
            <button class='place-card__like-icon'></button>
        </div>
    </div>
    */

    // Создаём главный div place-card
    const placeCard = document.createElement('div');
    placeCard.classList.add('place-card');

    // Создаём дочерние эл-ты

    // создаём place-card__delete-icon
    const btnDeleteIcon = document.createElement('button');
    btnDeleteIcon.classList.add('place-card__delete-icon');

    // создаём place-card__image
    const divImage = document.createElement('div');
    divImage.setAttribute('style', `background-image:url(${link})`);
    divImage.classList.add('place-card__image');
    divImage.appendChild(btnDeleteIcon);            // добавляем дочерний элемент place-card__delete-icon

    // создаём place-card__name
    const h3Name = document.createElement('h3');
    h3Name.classList.add('place-card__name');
    h3Name.textContent = `${name}`;

    // создаём place-card__like-icon
    const btnLikeIcon = document.createElement('button');
    btnLikeIcon.classList.add('place-card__like-icon');

    // создаём place-card__description
    const divDescription = document.createElement('div');
    divDescription.classList.add('place-card__description');
    divDescription.appendChild(h3Name);             // добавляем дочерний элемент place-card__name
    divDescription.appendChild(btnLikeIcon);        // добавляем дочерний элемент place-card__like-icon

    placeCard.appendChild(divImage);                // добавляем дочерний элемент place-card__image
    placeCard.appendChild(divDescription);          // добавляем дочерний элемент place-card__description

    return placeCard;
}

function loadNewCards(paramsArray) {
    paramsArray.forEach(function(obj){
        placesList.appendChild(createCard(obj.name, obj.link));
    })
}
// ====================== 1. Десять карточек «из коробки» ======================

//Можно лучше: Стоит придерживаться следующей структуры кода
// Переменные
// Функции
// Обработчики
// Вызов функций
// это повысит читаемость кода, сделает его визуально более привлекательным и исключит ошибки с вызовом необъявленных переменных/функций.

loadNewCards(initialCards);
//Можно лучше: Функция сильно перегружена логикой. Стоит вынести логику из условий в отдельные функции, чтобы разделить ответтствунность и для возможности переиспользовать и легче поддерживать код.
root.addEventListener('click', function (event) {
    // todo: переписать в swich/case
    if (event.target.classList.contains('user-info__button')) {                 // 2. Открытие формы
        popup.classList.add('popup_is-opened');         
    } else if (event.target.classList.contains('popup__close')) {               // 3. Закрытие формы
        popup.classList.remove('popup_is-opened');      
    } else if (event.target.classList.contains('place-card__like-icon')) {      // 4. Лайк
        event.target.classList.toggle('place-card__like-icon_liked');
    } else if (event.target.classList.contains('place-card__delete-icon')) {    // 6. Удаление карточки
        // todo: двойное использование .parentElement выглядит костыльно - найти более элегантный способ
        var delPlaceCard = event.target.parentElement.parentElement
        placesList.removeChild(delPlaceCard);
    }
});

popupForm.addEventListener('submit', function (event) {                         // 5. Добавление карточки
    event.preventDefault();
    const name = popupForm.elements.name.value;
    const link = popupForm.elements.link.value;
    // на всякий случай чутка овалидирую )
    if (name && link) {   
        placesList.appendChild(createCard(name, link));
        popupForm.reset()                                                       // зачищаем форму дабы комфортно добавлять последующие карточки
        popup.classList.remove('popup_is-opened');
    }
});

document.addEventListener('keydown', function (event) {
    //Можно лучше: keyCode порождал проблемы и из-за этого признан устаревшим, лучше воспользоваться event.code
    //https://keycode.info/
    //https://learn.javascript.ru/keyboard-events#sobytiya-keydown-i-keyup
// закрываем форму по нажатию клавиши 'Esc'
    if (popup.classList.contains('popup_is-opened') && event.keyCode === 27) {
        // Можно лучше: открытие и закрытие попапа стоит вынести в функции для возможности повторного использования в коде.
        // Вместо classList.add и classList.remove мы можем воспользоваться classList.toggle для объединения логики в одну функцию.
        popup.classList.remove('popup_is-opened');
    }
});

//Можно лучше: Стоит очищать форму не только после добавления карточки, но и при закрытии попапа на крестик. Это своего рода кнопка отмены изменений. При повторном открытии форма должна быть пуста.
//Можно лучше: Сброс формы стоит вынести в отдельную функцию, принимающую на вход форму. Так можно сбрасывать одной функцией неограниченное количество форм.


/*Отлично:
Код хорошо структурирован и стилизован
Используются комментарии в коде
Используется делегирование
Функционал полностью соответствует заданию
Отсутствуют неиспользуемые функции
Добавле обработчик закрытия формы по Esc*/

//Просьба не оставлять без внимания не критичные комментарии. Рефакторинг - неотъемлемая часть работы программиста. Всегда нужно стараться делать код лучше.
//Перед отправкой на проверку следующего спринта необходимо очистить код от комментариев предыдущего спринта. Спасибо.
//Успехов в дальнейшем обучении.
