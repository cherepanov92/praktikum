'use strict';

//                                                               Переменные
const root = document.querySelector('.root');

// генерируем карточки
const cards = initialCards.map(function (cardData) {
  const newCard = new Card(cardData);
  return newCard.create();
});

const cardList = new CardList(document.querySelector('.places-list'), cards);
const domName = root.querySelector('.user-info__name');
const domJob = root.querySelector('.user-info__job');
const userInfo = new UserInfo(domName, domJob);

// Модальное окно "создание карточки"
const newCardBtn = root.querySelector('.user-info__button');
const newCardPopup = new Popup(templateNewCard, root);
const newCardForm = newCardPopup.popup.querySelector('.popup__form');
const newCardValidator = new FormValidator(newCardPopup.popup.querySelector('.popup__form'));

// Модальное окно "редактирование профиля"
const editProfileBtn = root.querySelector('.user-info__button_edit');
const editProfilePopup = new Popup(templateEditProfile, root);
const editProfileForm = editProfilePopup.popup.querySelector('.popup__form');
const editProfileValidator = new FormValidator(editProfileForm);

// Модальное окно "картинка карточки"
const ViewImageBtn = root.querySelector('.place-card__image');
const ViewImagePopup = new ImgPopup(templateViewImage, root);

//                                                               Функции

function addCardImageListeners() {
  cardList.cardContainer.addEventListener('click', (event) => {
    if (event.target.classList.contains('place-card__image')) {
      const backgroundUrl = event.target.style['background-image'];
      const imgUrl = backgroundUrl.slice(5, backgroundUrl.length - 2);
      ViewImagePopup.open(imgUrl);
    }
  })
}


function addNewCardValidator() {
  newCardValidator.setEventListeners();

  newCardForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const name = document.forms.newCard.elements.name.value;
    const link = document.forms.newCard.elements.link.value;
    const newCard = new Card({ "name": name, "link": link })
    cardList.addCard(newCard.create());
    addCardImageListeners();
    newCardPopup.close();
  });

  newCardBtn.addEventListener('click', (event) => {
    newCardPopup.open();
  });
}

function addEditProfileValidator() {
  editProfileValidator.setEventListeners();

  editProfileForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const newName = document.forms.editProfile.elements.userName.value;
    const newJob = document.forms.editProfile.elements.userJob.value;
    userInfo.setUserInfo(newName, newJob)
    userInfo.updateUserInfo();
    editProfilePopup.close();
  });

  editProfileBtn.addEventListener('click', (event) => {
    editProfilePopup.open();
    document.forms.editProfile.userName.value = userInfo.name;
    document.forms.editProfile.userJob.value = userInfo.job;
  });
}

//                                                              Вызов функций

cardList.render();
addCardImageListeners();
addNewCardValidator();
addEditProfileValidator();

// Добрый день
// ## Итог
// - Использованы ES6-классы.
// - В классах напрямую не создаются экземпляры других классов.
// - Каждый класс выполняет строго одну задачу. Всё, что относится к решению этой задачи, находится в классе.
// - Делегирование больше не используется. Обработчики добавлены именно тем элементам, события которых нужно отслеживать.
// - Ненужные обработчики удаляются.
// - Каждый класс описан в отдельном JS-файле.

// Также использовано наследование классов

// Работа принята

// От себя добавлю, что проще и удобнее было бы просто оперировать взятыми из HTML элементами, чем
// делать шаблоны в переменных.
